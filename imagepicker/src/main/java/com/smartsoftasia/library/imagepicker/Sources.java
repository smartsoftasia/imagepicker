package com.smartsoftasia.library.imagepicker;

public enum Sources {
  CAMERA, GALLERY, VIDEO, VIDEO_GALLERY
}