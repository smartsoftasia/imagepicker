package com.smartsoftasia.library.androidimagepicker;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.smartsoftasia.library.imagepicker.RxImageConverters;
import com.smartsoftasia.library.imagepicker.RxImagePicker;
import com.smartsoftasia.library.imagepicker.Sources;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ssa-dev-4 on 18/1/2560.
 */
public class MainActivity extends Activity {

  private Button cameraButton;
  private Button galleryButton;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    cameraButton = (Button) findViewById(R.id.button_camera);
    galleryButton = (Button) findViewById(R.id.button_gallery);

    cameraButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Observable.just(
            RxImagePicker.with(getApplicationContext())
                .requestImage(Sources.CAMERA)
                .flatMap(new Function<Uri, Observable<String>>() {
                  @Override
                  public Observable<String> apply(Uri uri) throws Exception {
                    return RxImageConverters.uriToFullPath(getApplicationContext(), uri);
                  }
                })).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
      }
    });

    galleryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Observable.just(
            RxImagePicker.with(getApplicationContext())
                .requestImage(Sources.GALLERY)
                .flatMap(new Function<Uri, ObservableSource<String>>() {
                  @Override
                  public ObservableSource<String> apply(Uri uri) throws Exception {
                    return RxImageConverters.uriToFullPath(getApplicationContext(), uri);
                  }
                })).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
      }
    });
  }
}